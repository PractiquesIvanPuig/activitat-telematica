package com.ipuig.complexos;

import java.util.Properties;
import java.io.FileOutputStream;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.FileNotFoundException;

public class Moble {

    private double ample;
    private double llarg;
    private int color; // Com el proposit no es treballar amb colors, declaro un String que sera mes visual
    public static int numeroMobles = 0;
    private final int ID;

    public Moble(double ample, double llarg, int color) {

        this.ample = ample;
        this.llarg = llarg;
        this.color = color;
        numeroMobles++;
        this.ID = numeroMobles;
    }

    public static Moble nouMoble(String path) {

        try(var in  = new FileInputStream(path)) {

            var properties = new Properties();
            properties.load(in);

            double ample = Double.parseDouble(properties.getProperty("ample"));
            double llarg = Double.parseDouble(properties.getProperty("llarg"));
            int color = Integer.parseInt(properties.getProperty("nom"));

            return new Moble(ample, llarg, color);
        }

        catch(FileNotFoundException e) {
            e.printStackTrace();

            double ample = -1;
            double llarg = -1;
            int color = -1;

            return new Moble(ample, llarg, color);
        }

        catch(IOException e) {
            e.printStackTrace();

            double ample = -1;
            double llarg = -1;
            int color = -1;

            return new Moble(ample, llarg, color);
        }
    }

    @Override
    public String toString() {
    	
    	return "[ Moble " + this.ID + " de color " + this.color + " mida " + this.ample + " x " + this.llarg + " ]";
    }

    public void guardarDades() {

        var properties = new Properties();

        properties.setProperty("ample", Double.toString(this.ample));
        properties.setProperty("llarg", Double.toString(this.llarg));
        properties.setProperty("color", Integer.toString(this.color));
        properties.setProperty("ID", Double.toString(this.ID));

        try(var fos = new FileOutputStream("Moble-" + this.ID + ".properties")){

            properties.store(fos, "properties del moble");
        }

        catch(IOException e) {
            e.printStackTrace();
        }

    }

    public int getID() {
    	return this.ID;
    }
}

