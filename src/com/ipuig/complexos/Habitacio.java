package com.ipuig.complexos;

import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.FileNotFoundException;

// LA HABITACIO HA DE SER LA MATEIXA
public class Habitacio {

	private static double ample;
	private static double llarg;
	private static String nom;
	private static ArrayList<Moble> mobles = new ArrayList<Moble>();

    public static void crearHabitacio() {

    	System.out.println("Introdueix el ample");
        ample = userDouble();

    	System.out.println("Introdueix el llarg");
        llarg = userDouble();

    	System.out.println("Introdueix el nom");
        nom = userString();

    }

    public static void crearHabitacio(double width, double height, String name, ArrayList<Moble> mob) {

        ample = width;
        llarg = height;
        nom = name;
        mobles = mob;

    }

	public static void carregarDades(String path) { // Carrega les properties

		try (var in = new FileInputStream(path)) {

			var properties = new Properties();
			properties.load(in);

			ample = Double.parseDouble(properties.getProperty("ample"));
			llarg = Double.parseDouble(properties.getProperty("llarg"));
			nom = properties.getProperty("nom");

			String[] mobles_carregats = properties.getProperty("mobles").split(",");

			for(String i: mobles_carregats) {
				afegirMoble(Moble.nouMoble("Moble-" + i.trim() + ".properties"));
			}

		}

		catch (FileNotFoundException e) {
			e.printStackTrace();

			ample = -1;
			llarg = -1;
			nom = "Undefined";
		}

		catch (IOException e) {
			e.printStackTrace();

			ample = -1;
			llarg = -1;
			nom = "Undefined";
		}

	}

	public static void afegirMoble(Moble moble) {

		mobles.add(moble);
	}

	public static ArrayList<Moble> getMobles() {

		return mobles;
	}

	public static void mostrarDades() {

		System.out.println("La habitacio amb el nom: " + nom + "\nMesura " + ample + " x " + llarg
				+ (mobles.isEmpty() ? "\nno te mobles" : "\nte " + Moble.numeroMobles + " mobles"));
	}

	public static void mostrarMobles() {
		mobles
			.forEach(System.out::println);
	}

	public static double userDouble() {
		var in = new Scanner(System.in);

		double value;

		try {

			value = in.nextDouble();
			return value;
		}

		catch(Exception e) {
			System.out.println("Introdueix un numero valid!");
			return userDouble();
		}
	}

	public static String userString() {
		var in = new Scanner(System.in);
		String value;

		try {
			value = in.next();
			return value;
		}

		catch(Exception e) {
			System.out.println("Introdueix caracters valids!");
			return userString();
		}



	}

	public static Moble crearMoble() {
		
		var in = new Scanner(System.in);

		System.out.println("Introdueix el ample");
		double amplada = userDouble();

		System.out.println("Introdueix la llargada");
		double llargada = userDouble();

		System.out.println("Introdueix el color");
		int color = (int) userDouble();

		System.out.println("## MOBLE AFEGIT ##");

		return new Moble(amplada, llargada, color);
	}

	public static void guardarDades() {

		var properties = new Properties();

		String width = Double.toString(ample);
		String heigth = Double.toString(llarg);
		String name = nom + "";
		ArrayList<Integer> mobleID = new ArrayList<>();

		properties.setProperty("ample", width);
		properties.setProperty("llarg", heigth);
		properties.setProperty("nom", name);

		mobles
			.forEach(moble -> {
				mobleID.add(moble.getID());
				moble.guardarDades();
			});

		properties.setProperty("mobles", mobleID.toString().replaceAll("\\[|\\]", ""));

		try (var fos = new FileOutputStream("Habitacio-" + nom + ".properties")) {

			properties.store(fos, "properties de l'habitacio");
		}

		catch (IOException e) {
			e.printStackTrace();
		}

	}

}
