package com.ipuig.complexos;

import java.util.Scanner;

public class Main {

    public static int inputUsuari() {

        try {
            var in = new Scanner(System.in);
            return in.nextInt();
        }

        catch(Exception e) {
        	System.out.println("Introdueix una opcio valida!");
        	return inputUsuari();
        	
        }
    }

    public static void interficie() {

        int opcio;

        do {

            System.out.println("Seleccioneu una opcio:"
                    + "\n 0.- Sortir"
                    + "\n 1.- Mostrar per pantalla les dades de l'habitacio"
                    + "\n 2.- Afegir un moble a l'habitacio"
                    + "\n 3.- Guardar a disc l'habitacio"
                    + "\n 4.- Llistar per pantalla tots els mobles de l'habitacio amb les seves dades"
                    + "\n 5.- Modifica la habitacio");

            opcio = inputUsuari();
            System.out.println("__________________________________________________________________\n");

    		switch(opcio) {

    		case 0:
    			System.out.println("Sortir");
    			break;
    		case 1:
    			System.out.println("Mostrar per pantalla les dades de l'habitacio");
                Habitacio.mostrarDades();
    		    break;
    		case 2:
    			System.out.println("Afegir un moble a l'habitacio");
    			Habitacio.afegirMoble(Habitacio.crearMoble());
    		    break;
    		case 3:
    			System.out.println("Guardar a disc l'habitacio");
                Habitacio.guardarDades();
    			break;
    		case 4:
    			if(Habitacio.getMobles().isEmpty()) {
    				System.out.println("No hi ha mobles per mostrar");
    			}

    			else {
    				Habitacio.mostrarMobles();
    			}

    			break;

    		case 5:
    			Habitacio.crearHabitacio();
    			break;

    		default:
    			break;
    		}
        }
        while(opcio != 0);
    }

    public static void main(String[] args) {

        interficie();

    }

}
